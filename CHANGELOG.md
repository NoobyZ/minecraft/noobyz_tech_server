### CHANGELOG / Version History:

* 2024-10-25 _ v2.1.0
> + Update:
> + + Applied Botanics Addon 1.0.4 -> 1.0.3
> + + Artifacts 4.2.4 -> 4.2.3
> + + Bonsai Trees 3 3.2.1 -> 3.1.0
> + + Cloth Config API (Fabric/Forge) 6.5.133 -> 6.5.116
> + + Corail Tombstone 7.7.1 -> 7.6.8
> + + Create 0.5.1.i -> 0.5.1.f
> + + Create Railways Navigator 0.6.0 -> 0.5.0-beta
> + + Cyclic 1.7.20 -> 1.7.19
> + + Dynmap-Forge/Fabric 3.7-beta-6 -> 3.7-beta-4
> + + Industrial Foregoing 3.3.1.7-11 -> 3.3.1.6-10
> + + Inventory HUD+ 3.4.26 -> 3.4.18
> + + JAOPCA 4.1.20.47 -> 4.1.19.41
> + + JEI - Just Enough Items 10.2.1.1008 -> 10.2.1.1006
> + + Lootr 0.3.29.72 -> 0.3.28.70
> + + Macaw's Bridges 3.0.1 -> 3.0.0
> + + Macaw's Doors 1.1.1 -> 1.1.0
> + + Macaw's Fences and Walls 1.1.2 -> 1.1.1
> + + Macaw's Lights and Lamps 1.1.0 -> 1.0.6
> + + Macaw's Roofs 2.3.1 -> 2.3.0
> + + Macaw's Trapdoors 1.1.3 -> 1.1.2
> + + Macaw's Windows 2.3.0 -> 2.2.1
> + + MineColonies 1.1.612-BETA -> 1.1.149-RELEASE
> + + Moonlight Lib 1.17.16 -> 1.17.14
> + + Puzzles Lib [Forge] 3.5.10 -> 3.5.9
> + + Repurposed Structures (Forge) 5.1.15 -> 5.1.14
> + + RFTools Dimensions 8.0.11 -> 8.0.10
> + + Simple Voice Chat 2.5.24 -> 2.5.13
> + + Sophisticated Backpacks 3.20.3.1063 -> 3.20.2.1036
> + + Sophisticated Storage 0.9.8.915 -> 0.9.7.764
> + + Tinkers Construct 3.7.2.167 -> 3.7.1.155
> + + Tinkers' Integrations and Tweaks 2.0.18.3 -> 2.0.18.1

> + Add:
> + + Advanced Peripherals

> + Change:
> + + config

* 2024-05-07 _ v2.0.5
> + Update:
> + + Create Railways Navigator 0.4.1-beta -> 0.5.0-beta
> + + FORGE 40.2.18 -> 40.2.21
> + + TerraBlender (Forge) 1.2.0.126 -> 1.1.0.102

> + Change:
> + + config

* 2024-05-04 _ v2.0.0
> + Update:
> + + Cyclic 1.7.17 -> 1.7.19
> + + JAOPCA 4.1.18.40 -> 4.1.19.41
> + + Journeymap 5.9.7p1 -> 5.9.8
> + + Just Enough Breeding (JEBr) 1.0.9 -> 1.2.1
> + + liblPN 4.0.1 -> 4.0.2
> + + Lootr 0.3.27.69 -> 0.3.28.70
> + + Macaw's Bridges 2.1.0 -> 3.0.0
> + + Polymorph 0.49 -> 0.50
> + + Sophisticated Core 0.6.4.586 -> 0.6.4.604

> + Add:
> + + Chunk Loaders 1.2.8a
> + + Create Deco 1.3.3
> + + Create Railways Navigator 0.4.1-beta
> + + Create: Steam 'n' Rails 1.4.8
> + + Dynmap-Forge/Fabric 3.7-beta-4
> + + DynmapBlockScan 3.6
> + + MySQL JDBC (Fabric/Forge) 8.0.33+20230506-all
> + + Simple Voice Chat 2.5.13
> + + Thermal Dynamics 9.2.2.19
> + + Thermal Integration 9.2.1.18

> + Delete:
FTB Chunks (Forge)
FTB Library (Forge)
FTB Teams (Forge)

> + Change:
> + + Configs

* 2024-03-24 _ v1.2.1
> + Update:
> + + Corail Tombstone 7.6.7 -> 7.6.8
> + + FORGE 40.2.17 -> 40.2.18
> + + Sophisticated Backpacks 3.20.2.1026 -> 3.20.2.1036
> + + Sophisticated Core 0.6.3.565 -> 0.6.4.586
> + + Sophisticated Storage 0.9.7.742 -> 0.9.7.764

> + Change:
> + + configs

* 2024-03-10 _ v1.2.0
> + Update:
> + + FORGE 40.2.14 -> 40.2.17
> + + JAOPCA 4.1.16.32 -> 4.1.18.40
> + + Journeymap 5.9.7 -> 5.9.7p1
> + + Lootr 0.3.25.64 -> 0.3.27.69
> + + Sophisticated Backpacks 3.19.4.976 -> 3.20.2.1026
> + + Sophisticated Core 0.5.109.515 -> 0.6.3.565
> + + Sophisticated Storage 0.8.53.669 -> 0.9.7.742

> + Add:
> + + Ars Creo 2.2.0
> + + Ars Scalaes 1.8.1

> + Change:
> + + configs

* 2024-03-04 _ v1.1.0
> + Update:
> + + Architectury API (Fabric/Forge) 4.11.93 -> 4.12.94
> + + Artifacts 4.2.2 -> 4.2.3
> + + Botany Pots 8.1.30 -> 8.1.32
> + + Cloth Config API (Fabric/Forge) 6.5.102 -> 6.5.116
> + + Corail Tombstone 7.6.6 -> 7.6.7
> + + Create: Applied Kinetics 1.3.1 -> 1.3.2
> + + Curios API (Forge) 5.0.9.1 -> 5.0.9.2
> + + Jade 5.3.1 -> 5.3.2
> + + JAOPCA 4.1.16.32 -> 4.1.18.39
> + + JEI - Just Enough Items 10.2.1.1005 -> 10.2.1.1006
> + + Lootr 0.3.25.64 -> 0.3.26.68
> + + Macaw's Fences and Walls 1.0.7 -> 1.1.1
> + + Macaw's Roofs 2.2.4 -> 2.3.0
> + + Macaw's Windows 2.2.0 -> 2.2.1
> + + Mantle 1.9.45 -> 1.9.50
> + + Max Health Fix 5.0.4 -> 5.0.5
> + + Puzzles Lib [Forge] v3.5.8 -> v3.5.9
> + + RFTools Builder 4.1.3 -> 4.1.4
> + + Sophisticated Backpacks 3.19.4.976 -> 3.20.1.1011
> + + Sophisticated Core 0.5.109.515 -> 0.6.2.551
> + + Sophisticated Storage 0.8.53.669 -> 0.9.5.725
> + + SuperMartijn642's Core Lib 1.1.16 -> 1.1.17
> + + Tinkers Construct 3.6.4.113 -> 3.7.1.155
> + + titanium 3.5.11-45 -> 3.5.11-47

> + Add:
> + + Ars Nouveau 2.9.0
> + + Hexal 0.1.14
> + + Hex Casting 0.9.6
> + + The Twilight Forest 4.1.1494-universal

* 2023-12-15 _ v1.0.5
> + ReUpdate:
> + + Beyond Earth 7.0-SNAPSHOT -> 6.2

* 2023-12-14 _ v1.0.4
> + Update:
> + + AE2 Things [Forge] 1.0.5 -> 1.0.7
> + + AppleSkin 2.4.1 -> 2.5.1
> + + Applied Energistics 2 11.7.5 -> 11.7.6
> + + Beyond Earth 6.2 -> 7.0-SNAPSHOT
> + + Botany Pots 8.1.29 -> 8.1.30
> + + Brandons Core 3.1.9.280-universal -> 3.1.10.283-universal
> + + CoFH Core 9.2.1.39 -> 9.2.3.47
> + + Corail Tombstone 7.6.5 -> 7.6.6
> + + Create 0.5.1.c -> 0.5.1.f
> + + Cucumber 5.1.4 -> 5.1.5
> + + Draconic-Evolution 3.0.29.524 -> 3.0.31.531
> + + FORGE 40.2.10 -> 40.2.14
> + + Jade 5.3.0 -> 5.3.1
> + + JAOPCA 4.1.15.28 -> 4.1.16.32
> + + Just Enough Breeding (JEBr) 1.0.6 -> 1.0.9
> + + Just Enough Mekanism Multiblocks 2.2 -> 2.4
> + + KubeJS Create 2.4-build.5 -> 2.4-build.16
> + + KubeJS Thermal 1.5-build.16 -> 1.6-build.7
> + + liblPN 4.0.0 -> 4.0.1
> + + Lootr 0.3.25.63 -> 0.3.25.64
> + + Macaw's Trapdoors 1.1.1 -> 1.1.2
> + + Macaw's Windows 2.1.1 -> 2.2.0
> + + MineColonies 1.1.29-BETA -> 1.1.149-RELEASE
> + + Moonlight Lib 1.17.12 -> 1.17.14
> + + Pam's HarvestCraft 2 - Crops 1.0.5 -> 1.0.6
> + + PneumaticCraft: Repressurized 3.6.1-29 -> 3.6.4-45
> + + RFTools Utility 4.0.23 -> 4.0.24
> + + Sophisticated Backpacks 3.18.57.888 -> 3.19.4.976
> + + Sophisticated Core 0.5.82.393 -> 0.5.109.515
> + + Sophisticated Storage 0.8.39.580 -> 0.8.53.669
> + + SuperMartijn642's Core Lib 1.1.12 -> 1.1.16
> + + Tesseract 1.0.35 -> 1.0.35a
> + + Thermal Expansion 9.2.0.20 -> 9.2.2.24
> + + Thermal Foundation 9.2.0.46 -> 9.2.2.58
> + + Thermal Innovation 9.2.0.17 -> 9.2.1.19
> + + Thermal Locomotion 9.2.0.13 -> 9.2.1.15
> + + titanium 3.5.9-43 -> 3.5.11-45

* 2023-09-01 _ v1.0.3
> + Add:
> + + Tinkers Disassembler 1.2.0
> + + configs

> + Change:
> + + configs

* 2023-08-29 _ v1.0.1
> + Update:
> + + Applied Energistics 2 11.7.4 -> 11.7.5
> + + Just Enough Breeding (JEBr) 1.0.0 -> 1.0.6
> + + Moonlight Lib 1.17.11 -> 1.17.12

* 2023-08-21 _ v1.0.0

> + Update:
> + + Botany Pots 8.1.28 -> 8.1.29
> + + FluxNetworks 7.0.8.12 -> 7.0.9.15
> + + liblPN 3.0.2 -> 4.0.0
> + + SuperMartijn642's Config Lib 1.1.7 -> 1.1.8

> + Add:
> + + Create: Applied Kinetics 1.3.1
> + + Create Recycle Everything 1.0.2
> + + Curious Lanterns 1.3.3
> + + FerriteCore (Forge) 4.2.2
> + + Just Enough Breeding (JEBr) 1.0.0
> + + Just Enough Immersive Multiblocks 0.0.2
> + + Just Enough Mekanism Multiblocks 2.2
> + + Not Enough Crashes (Forge) 4.2.0
> + + spark 1.10.38

* 2023-08-17 _ v0.7.6
> + Delete:
> + + Flywheel (dont need anymore)

* 2023-08-16 _ v0.7.5
> + Update:
> + + Apotheosis 5.7.7 -> 5.8.1
> + + Applied Energistics 2 11.7.0 -> 11.7.4
> + + Architectury API (Fabric/Forge) 4.11.90 -> 4.11.93
> + + Artifacts 4.2.1 -> 4.2.2
> + + Bookshelf 13.2.53 -> 13.3.56
> + + CC: Tweaked 1.101.2 -> 1.101.3
> + + Cloth Config API (Fabric/Forge) 6.4.90 -> 6.5.102
> + + CodeChickenLib 4.1.3.480 -> 4.1.4.488
> + + Controlling 9.0+22 -> 9.0+23
> + + Corail Tombstone 7.6.4 -> 7.6.5
> + + Create 0.5.0.i -> 0.5.1.c
> + + Create Crafts & Additions 20230411a -> 1.0.0
> + + Cucumber 5.1.3 -> 5.1.4
> + + Curios API (Forge) 5.0.9.0 -> 5.0.9.1
> + + Curious Armor Stands 4.0.0 -> 4.0.1
> + + Cyclic 1.7.14 -> 1.7.17
> + + Dark Mode Everywhere 1.0.2 -> 1.1.3
> + + DarkUtilities 10.1.6 -> 10.1.7
> + + Dimensional Dungeons 172 -> 178
> + + Extreme Reactors 2.0.61 -> 2.0.70
> + + FORGE 40.2.1 -> 40.2.10
> + + FTB Chunks (Forge) 3.16-build.247 -> 3.17-build.265
> + + FTB Teams (Forge) 2.10-build.96 -> 2.11-build.107
> + + Hostile Neural Networks 3.2.2 -> 3.3.0
> + + Inventory HUD+ 3.4.10 -> 3.4.18
> + + Jade 5.2.6 -> 5.3.0
> + + Jade Addons 2.4.1 -> 2.5.0
> + + JEI - Just Enough Items 10.2.1.1004 -> 10.2.1.1005
> + + JER - Integration 3.3.2 -> 3.4.0
> + + Journeymap 5.9.5 -> 5.9.7
> + + JourneyMap Integration 0.13-32 -> 0.13-43
> + + Kotlin for Forge 3.11.0 -> 3.12.0
> + + KubeJS Create 2.2-build.28 -> 2.4-build.5
> + + liblPN 2.0.4 -> 3.0.2
> + + Little Logistics 1.2.6 -> 1.2.7
> + + Lootr 0.2.24.61 -> 0.3.25.63
> + + Macaw's Bridges 2.0.7 -> 2.1.0
> + + Macaw's Doors 1.0.9 -> 1.1.0
> + + Macaw's Lights and Lamps 1.0.5 -> 1.0.6
> + + Macaw's Roofs 2.2.3 -> 2.2.4
> + + Macaw's Trapdoors 1.0.9 -> 1.1.1
> + + Mantle 1.9.43 -> 1.9.45
> + + MineColonies 1.0.1359-ALPHA -> 1.1.29-BETA
> + + Mob Grinding Utils 0.4.48 -> 0.4.50
> + + Moonlight Lib 1.17.9 -> 1.17.11
> + + MysticalAgradditions 5.1.3 -> 5.1.4
> + + MysticalAgriculture 5.1.4 -> 5.1.5
> + + NoMoWanderer 1.1.5 -> 1.1.6
> + + Placebo 6.6.6 -> 6.6.7
> + + PneumaticCraft: Repressurized 3.6.0-19 -> 3.6.1-29
> + + Polymorph 0.46 -> 0.49
> + + Puzzles Lib [Forge] v3.3.6 -> v3.5.7
> + + RFTools Builder 4.1.1 -> 4.1.3
> + + Sophisticated Backpacks 3.18.44.814 -> 3.18.57.888
> + + Sophisticated Core 0.5.48.244 -> 0.5.82.393
> + + Sophisticated Storage 0.6.25.324 -> 0.8.39.580
> + + SuperMartijn642's Config Lib 1.1.6 -> 1.1.7
> + + SuperMartijn642's Core Lib 1.1.7 -> 1.1.12
> + + Supplementaries 1.5.16 -> 1.5.18
> + + Tesseract 1.0.29 -> 1.0.35
> + + Tinkers Construct 3.6.3.111 -> 3.6.4.113
> + + Tinkers' Integrations and Tweaks 2.0.15.0 -> 2.0.18.1
> + + Tinkers' Reforged 2.0.5 -> 2.0.8
> + + Torchmaster 18.1.0 -> 18.2.1
> + + Trashcans 1.0.17a -> 1.0.18
> + + Upgraded Netherite 4.3.0.0-release -> 4.3.0.1-release
> + + ZeroCore2 2.1.31 -> 2.1.39

> + Add:
> + + Max Health Fix 5.0.4

* 2023-04-15 _ v0.7.4
> + Update:
> + + Applied Botanics Addon 1.0.2-alpha -> 1.0.3
> + + Architectury API (Fabric/Forge) 4.11.89 -> 4.11.90
> + + Corail Tombstone 7.6.3 -> 7.6.4
> + + Create Crafts & Additions 20230315b -> 20230411a
> + + Easier Sleeping 2.1.0 -> 2.1.3
> + + Journeymap 5.9.4 -> 5.9.5
> + + MineColonies 1.0.1305-ALPHA -> 1.0.1359-ALPHA
> + + Mob Grinding Utils 0.4.47 -> 0.4.48
> + + Sophisticated Backpacks 3.18.43.802 -> 3.18.44.814
> + + Sophisticated Core 0.5.45.229 -> 0.5.48.244
> + + Sophisticated Storage 0.6.22.308 -> 0.6.25.324
> + + SuperMartijn642's Core Lib 1.1.6 -> 1.1.7
> + + Tinkers' Reforged 2.0.4 -> 2.0.5
> + + titanium 3.5.6-40 -> 3.5.9-43

> + Delete:
> + + Planeteer - Core
> + + Planeteer - Machines
> + + Planeteer - Dimensions

> + Change:
> + + configs

* 2023-04-02 _ v0.7.3
> + Change:
> + + Fix the crashes

* 2023-04-02 _ v0.7.0
> + Update:
> + + Configured 2.0.0 -> 2.0.1
> + + Create Crafts & Additions 20230221a -> 20230315b
> + + Curios API (Forge) 5.0.8.0 -> 5.0.9.0
> + + Dimensional Dungeons 168 -> 172
> + + Draconic-Evolution 3.0.28.516 -> 3.0.29.524
> + + Extreme Reactors 2.0.60 -> 2.0.61
> + + FTB Chunks (Forge) 3.13-build.234 -> 3.16-build.247
> + + Inventory HUD+ 3.4.4 -> 3.4.10
> + + JAOPCA 4.1.14.26 -> 4.1.15.28
> + + JEI - Just Enough Items 10.2.1.1002 -> 10.2.1.1004
> + + JER - Just Enough Resources 0.14.1.171 -> 0.14.2.206
> + + Journeymap 5.9.3 -> 5.9.4
> + + JourneyMap Integration 0.13-31 -> 0.13-32
> + + Kotlin for Forge 3.10.0 -> 3.11.0
> + + liblPN 2.0.2 -> 2.0.4
> + + Lootr 0.2.23.60 -> 0.2.24.61
> + + Macaw's Bridges 2.0.6 -> 2.0.7
> + + Macaw's Doors 1.0.8 -> 1.0.9
> + + Macaw's Roofs 2.2.2 -> 2.2.3
> + + Macaw's Trapdoors 1.0.8 -> 1.0.9
> + + MineColonies 1.0.1251-ALPHA -> 1.0.1305-ALPHA
> + + RFTools Dimensions 8.0.9 -> 8.0.10
> + + SebastrnLib 1.0.1 -> 1.0.2
> + + Sophisticated Backpacks 3.18.40.777 -> 3.18.43.802
> + + Sophisticated Core 0.5.37.202 -> 0.5.45.229
> + + Sophisticated Storage 0.6.16.274 -> 0.6.22.308
> + + Structurize 1.0.424-ALPHA -> 1.0.448-ALPHA
> + + SuperMartijn642's Core Lib 1.1.4 -> 1.1.6
> + + Tinkers' Reforged 2.0.2 -> 2.0.4
> + + Trashcans 1.0.17 -> 1.0.17a
> + + Xnet 4.0.85 -> 4.0.9
> + + YungsApi 2.2.7 -> 2.2.9
> + + ZeroCore2 2.1.30 -> 2.1.31

> + Add:
> + + [Forge] AE2 Additional Opportunity
> + + AE2 Things [Forge]
> + + AE2-Additions
> + + Applied Botanics Addon
> + + Applied Cooking
> + + Applied Energistics 2
> + + Applied Energistics 2 Wireless Terminals
> + + Applied Mekanistics
> + + Beyond Earth
> + + Beyond Earth: Giselle Addon
> + + Chisels & Bits - For Forge
> + + Cloth Config API (Fabric/Forge)
> + + Corail Tombstone
> + + JEI Integration
> + + Lazier AE2
> + + Planeteer - Core
> + + Planeteer - Dimensions
> + + Planeteer - Machines

> + Change:
> + + configs

> + Delete:
> + + Cable Tiers
> + + Dimensional Pockets II
> + + Extra Disks
> + + Extra Storage
> + + GraveStone Mod
> + + LaserIO
> + + mutil
> + + Refined Cooking
> + + Refined Storage
> + + Refined Storage Addons
> + + Refined Storage: Requestify
> + + Simple Magnets
> + + Simple Storage Network
> + + Tetra
> + + TheObsidianBoat
> + + Thermal Cultivation
> + + Time in a bottle standalone

* 2023-02-26 _ v0.6.1
> + Update:
> + + Immersive Engineering 8.3.1-157 -> 8.4.0-161

* 2023-02-26 _ v0.6.0
> + Update:
> + + Balm (Forge Edition) 3.2.2+0 -> 3.2.3+0
> + + NetherPortalFix 9.0.0 -> 9.0.1
> + + Puzzles Lib [Forge] v3.3.5 -> v3.3.6
> + + SuperMartijn642's Core Lib 1.1.1a -> 1.1.4
> + + Tinkers' Reforged 2.0.0 -> 2.0.2
> + + Upgraded Core 3.2.0.0-release -> 3.3.0.0-release
> + + Upgraded Netherite 4.2.0.6-release -> 4.3.0.0-release

> + Add:
> + + Brandons Core 3.1.8.277
> + + CodeChickenLib 4.1.3.480
> + + Draconic-Evolution 3.0.28.516

* 2023-02-22 _ v0.5.0
> + Update:
> + + Architectury API (Fabric/Forge) 4.10.88 -> 4.11.89
> + + BlockUI 0.0.62-ALPHA -> 0.0.71-ALPHA
> + + Botany Pots 8.0.24 -> 8.1.26
> + + Botany Pots Tiers 2.1.2 -> 2.2.0
> + + CC: Tweaked 1.101.0 -> 1.101.2
> + + CoFH Core 9.1.2.32 -> 9.2.1.39
> + + Comforts (Forge) 5.0.0.5 -> 5.0.0.6
> + + Convenient Curios Container 1.11 -> 1.12
> + + Create Crafts & Additions 20221219a -> 20230221a
> + + Curios API (Forge) 5.0.7.1 -> 5.0.8.0
> + + Cyclic 1.7.13 -> 1.7.14
> + + FTB Chunks (Forge) 3.12-build.217 -> 3.13-build.234
> + + FTB Library (Forge) 3.9-build.167 -> 3.11-build.177
> + + FTB Teams (Forge) 2.9-build.88 -> 2.10-build.96
> + + Industrial Foregoing 3.3.1.5-9 -> 3.3.1.6-10
> + + JAOPCA 4.1.13.23 -> 4.1.14.26
> + + Journeymap 5.9.2 -> 5.9.3
> + + Kotlin for Forge 3.9.1 -> 3.10.0
> + + KubeJS 5.5-build.567 -> 5.5-build.569
> + + Macaw's Doors 1.0.7 -> 1.0.8
> + + MineColonies 1.0.1208-ALPHA -> 1.0.1251-ALPHA
> + + MysticalAgriculture 5.1.3 -> 5.1.4
> + + Pick Up Notifier [Forge] v3.2.0 -> v3.2.1
> + + PneumaticCraft: Repressurized 3.5.0-17 -> 3.6.0-19
> + + Refined Storage 1.10.4 -> 1.10.5
> + + RFTools Control 5.0.9 -> 5.0.10
> + + RFTools Utility 4.0.22 -> 4.0.23
> + + SuperMartijn642's Core Lib 1.1.1a -> 1.1.3a
> + + Supplementaries 1.5.15 -> 1.5.16
> + + Tesseract 1.0.28a -> 1.0.29
> + + Thermal Cultivation 9.1.0.14 -> 9.2.0.16
> + + Thermal Expansion 9.1.0.18 -> 9.2.0.20
> + + Thermal Foundation 9.1.0.34 -> 9.2.0.46
> + + Thermal Innovation 9.1.0.15 -> 9.2.0.17
> + + Thermal Locomotion 9.1.0.11 -> 9.2.0.13
> + + Tinkers Construct 3.6.2.92 -> 3.6.3.111
> + + Tinkers' Reforged 2.0.0 -> 2.0.1
> + + Trashcans 1.0.16 -> 1.0.17
> + + Xnet 4.0.7 -> 4.0.85

* 2023-01-31 _ v0.4.2
> + Add:
> + + SebastrnLib 1.0.1

* 2023-01-31 _ v0.4.1
> + Update:
> + + Balm (Forge Edition) 3.2.1+0 -> 3.2.2+0
> + + Botany Pots Tiers 2.1.0 -> 2.1.2
> + + Create 0.5.0.h -> 0.5.0.i
> + + Immersive Engineering 8.3.0-155 -> 8.3.1-157
> + + JAOPCA 4.1.12.22 -> 4.1.13.23
> + + liblPN 2.0.1 -> 2.0.2
> + + MineColonies 1.0.1197-ALPHA -> 1.0.1208-ALPHA
> + + Mob Grinding Utils 0.4.46 -> 0.4.47
> + + Pam's HarvestCraft 2 - Trees 1.0.3 -> 1.0.4
> + + RFTools Base 3.0.11 -> 3.0.12
> + + RFTools Control 5.0.8 -> 5.0.9
> + + RFTools Utility 4.0.21 -> 4.0.22
> + + SuperMartijn642's Core Lib 1.1.1 -> 1.1.1a
> + + Tesseract 1.0.28 -> 1.0.28a
> + + Tinkers Construct 3.6.1.88 -> 3.6.2.92

> + Add:
> + + Refined Cooking 2.0.3
> + + YUNG's Better Ocean Monuments (Forge) 1.0.3

> + Change:
> + + configs

* 2023-01-21 _ v0.4.0
> + Update:
> + + Apotheosis 5.7.0 -> 5.7.7
> + + Architectury API (Fabric/Forge) 4.9.84 -> 4.10.88
> + + Balm (Forge Edition) 3.2.0+0 -> 3.2.1+0
> + + Bookshelf 13.2.50 -> 13.2.52
> + + Building Gadgets 3.13.0-build.5 -> 3.13.1-build.18
> + + CC: Tweaked 1.100.10 -> 1.101.0
> + + Champions 2.1.6.0 -> 2.1.6.3
> + + Chipped 2.0.0 -> 2.0.1
> + + Clumps 8.0.0+15 -> 8.0.0+17
> + + CoFH Core 9.0.0.24 -> 9.1.2.32
> + + Compressium 1.4.1-build.6 -> 1.4.2-build.9
> + + Convenient Curios Container 1.5 -> 1.11
> + + CookingForBlockheads 12.1.1 -> 12.2.0
> + + Create 0.5.0.e -> 0.5.0.h
> + + Create Crafts & Additions 20220914a -> 20221219a
> + + Cyclic 1.7.9 -> 1.7.13
> + + DarkUtilities 10.0.5 -> 10.1.6
> + + Dimensional Dungeons 166 -> 168
> + + Domum Ornamentum 1.0.55-ALPHA -> 1.0.77-ALPHA
> + + Engineer's Decor 1.1.25 -> 1.1.28
> + + Extra Storage 2.2.0 -> 2.2.1
> + + Extreme Reactors 2.0.55 -> 2.0.60
> + + FluxNetworks 7.0.7.8 -> 7.0.8.12
> + + Flywheel 0.6.5 -> 0.6.8.a
> + + FORGE 40.1.84 -> 40.2.0
> + + FTB Chunks (Forge) 3.6-build.170 -> 3.12-build.217
> + + FTB Library (Forge) 3.6-build.123 -> 3.9-build.167
> + + FTB Teams (Forge) 2.6-build.69 -> 2.9-build.88
> + + Immersive Engineering 8.2.2-154 -> 8.3.0-155
> + + Iron Furnaces [FORGE] 3.3.2 -> 3.3.3
> + + Jade 5.2.4 -> 5.2.6
> + + Jade Addons 2.4.0 -> 2.4.1
> + + JAOPCA 4.1.10.16 -> 4.1.12.22
> + + JEI - Just Enough Items 10.1.4.263 -> 10.2.1.1002
> + + JEP - Just Enough Professions 1.2.2 -> 1.3.0
> + + Journeymap 5.9.0beta2 -> 5.9.2
> + + Kotlin for Forge 3.7.1 -> 3.9.1
> + + KubeJS 5.5-build.550 -> 5.5-build.567
> + + KubeJS Thermal 1.5-build.14 -> 1.5-build.16
> + + LaserIO 1.4.3 -> 1.4.5
> + + liblPN 1.0.3 -> 2.0.1
> + + Lootr 0.2.21.58 -> 0.2.23.60
> + + Macaw's Bridges 2.0.5 -> 2.0.6
> + + Macaw's Fences and Walls 1.0.6 -> 1.0.7
> + + Macaw's Lights and Lamps 1.0.4 -> 1.0.5
> + + Macaw's Roofs 2.2.1 -> 2.2.2
> + + Macaw's Trapdoors 1.0.7 -> 1.0.8
> + + Macaw's Windows 2.0.3 -> 2.1.1
> + + Mantle 1.9.27 -> 1.9.43
> + + mcjtylib 6.0.17 -> 6.0.20
> + + MineColonies 1.0.1072-ALPHA -> 1.0.1197-ALPHA
> + + More Minecarts and Rails 1.4.6 -> 1.4.7
> + + MysticalAgradditions 5.1.2 -> 5.1.3
> + + MysticalAgriculture 5.1.2 -> 5.1.3
> + + Pam's HarvestCraft 2 - Crops 1.0.4 -> 1.0.5
> + + Placebo 6.6.5 -> 6.6.6
> + + PneumaticCraft: Repressurized 3.4.3-8 -> 3.5.0-17
> + + Polymorph 0.45 -> 0.46
> + + Productive Bees 0.9.1.6 -> 0.9.3.0
> + + Refined Storage 1.10.3 -> 1.10.4
> + + RFTools Builder 4.0.16 -> 4.1.1
> + + RFTools Power 4.0.7 -> 4.0.9
> + + RFTools Storage 3.0.11 -> 3.0.12
> + + RFTools Utility 4.0.19 -> 4.0.21
> + + Rhino 2.1-build.240 -> 2.1-build.255
> + + Simple Magnets 1.1.8 -> 1.1.9
> + + Sophisticated Backpacks 3.18.29.718 -> 3.18.40.777
> + + Sophisticated Core 0.5.13.132 -> 0.5.37.202
> + + Sophisticated Storage 0.5.17.152 -> 0.6.16.274
> + + Structurize 1.0.424-ALPHA -> 1.0.424-ALPHA
> + + SuperMartijn642's Core Lib 1.0.19 -> 1.1.1
> + + Supplementaries 1.5.10 -> 1.5.15
> + + Tesseract 1.0.27 -> 1.0.28
> + + Thermal Cultivation 9.0.0.13 -> 9.1.0.14
> + + Thermal Expansion 9.0.0.15 -> 9.1.0.18
> + + Thermal Foundation 9.0.0.32 -> 9.1.0.34
> + + Thermal Innovation 9.0.0.14 -> 9.1.0.15
> + + Thermal Locomotion 9.0.0.10 -> 9.1.0.11
> + + Tinkers Construct 3.5.2.40 -> 3.6.1.88
> + + Tinkers' Integrations and Tweaks 2.0.14.2 -> 2.0.15.0
> + + Tinkers' Reforged 1.1.3 -> 2.0.0
> + + titanium 3.5.6-38 -> 3.5.6-40
> + + Toolbelt 1.18.8 -> 1.18.9
> + + Trashcans 1.0.15 -> 1.0.16
> + + XP Tome 2.1.6 -> 2.1.7
> + + YungsApi 2.2.4 -> 2.2.7
> + + ZeroCore2 2.1.26 -> 2.1.30

> + Add:
> + + Botany Pots
> + + Botany Pots Tiers
> + + Easier Sleeping
> + + NoMoWanderer
> + + YUNG's Better Ocean Monuments (Forge)
> + + YUNG's Better Desert Temples (Forge)

* 2022-10-19 _ v0.3.1
> + Change:
> + + configs

* 2022-10-19 _ v0.3.0

> + Update:
> + + Convenient Curios Container 1.4 -> 1.5
> + + YungsApi 2.0.8 -> 2.2.4

> + Delete:
> + + Alex's Delight
> + + Aquaculture 2
> + + Blueprint
> + + Compat O' Plenty
> + + Corn Delight
> + + Croptopia [FABRIC/FORGE]
> + + Farmer's Delight
> + + Farmer's Respite
> + + Gobber Delight (A Farmer's Delight Add-on)
> + + Materialis
> + + Mysterious Mountain Lib
> + + Nether's Delight
> + + Potions Master
> + + The Veggie Way [FORGE]

> + Change:
> + + configs

* 2022-10-17 _ v0.2.1

> + Change:
> + + server configs

* 2022-10-17 _ v0.2.0

> + Add and Change:
> + + server configs

> + Update:
> + + FastWorkbench 6.1.0 -> 6.1.1
> + + FORGE 40.1.80 -> 40.1.84
> + + Gobber Delight (A Farmer's Delight Add-on) 1.0.2 -> 1.0.3
> + + Macaw's Roofs 2.2.0 -> 2.2.1
> + + Supplementaries 1.5.9 -> 1.5.10
> + + Tetra 4.10.0 -> 4.10.1

> + Delete:
> + + Additional Lanterns
> + + Inventory Sorter
> + + JEA - Just Enough Advancements
> + + Macaw's Bridges - Biome O' Plenty
> + + Macaw's Roofs - Biomes O' Plenty
> + + Tool's Complement

* 2022-10-14 _ v0.1.01a

> + fix client mod not servermod

* 2022-10-14 _ v0.1.01

> + Update:
> + + Alex's Delight 1.3.2 -> 1.3.3
> + + Amethyst Tools Mod 1.6.2 -> 1.6.3
> + + Apotheosis 5.6.1 -> 5.7.0
> + + Architectury API (Fabric/Forge) 4.9.83 -> 4.9.84
> + + Artifacts 4.1.0 -> 4.2.0
> + + BlockUI 0.0.58-ALPHA -> 0.0.62-ALPHA
> + + Blueprint 5.4.4 -> 5.5.0
> + + Bookshelf 13.2.49 -> 13.2.50
> + + CC: Tweaked 1.100.9 -> 1.100.10
> + + Champions 2.1.5.6 -> 2.1.6.0
> + + Chipped 1.2.1 -> 2.0.0
> + + CoFH Core 1.6.4.21 -> 9.0.0.24
> + + Comforts (Forge) 5.0.0.4 -> 5.0.0.5
> + + Compat O' Plenty 1.5.4 -> 2.0.0
> + + Configured 1.5.4 -> 2.0.0
> + + Convenient Curios Container 1.2 -> 1.4
> + + CookingForBlockheads 12.0.2 -> 12.1.1
> + + Corn Delight 1.0.5 -> 1.0.6
> + + Create 0.5.0 -> 0.5.0.e
> + + Create Crafts & Additions 20220817a -> 20220914a
> + + Cucumber 5.1.2 -> 5.1.3
> + + Cyclic 1.7.7 -> 1.7.9
> + + Dimensional Pockets II 6.1.2.0 -> 6.1.4.0
> + + Enchanting Infuser [Forge] 3.3.2 -> 3.3.3
> + + Engineer's Decor 1.1.24 -> 1.1.25
> + + Explorer's Compass 1.2.3 -> 1.3.0
> + + Extreme Reactors 2.0.51 -> 2.0.55
> + + Farmer's Delight 1.1.2 -> 1.2.0
> + + FORGE 40.1.74 -> 40.1.80
> + + FTB Chunks (Forge) 3.6-build.147 -> 3.6-build.170
> + + FTB Teams (Forge) 2.6-build.51 -> 2.6-build.69
> + + Gobber [FORGE] 2.6.33 -> 2.6.37
> + + Hostile Neural Networks 3.2.1 -> 3.2.2
> + + Immersive Engineering 8.0.2-151 -> 8.2.2-154
> + + Iron Furnaces [FORGE] 3.3.1 -> 3.3.2
> + + JAOPCA 4.1.8.14 -> 4.1.10.16
> + + JEI - Just Enough Items 10.1.3.240 -> 10.1.4.263
> + + JourneyMap Integration 0.13-25 -> 0.13-31
> + + KubeJS 5.4-build.541 -> 5.5-build.550
> + + Lootr 0.2.19.56 -> 0.2.21.58
> + + Macaw's Bridges 2.0.3 -> 2.0.5
> + + Macaw's Roofs 2.1.2 -> 2.2.0
> + + Macaw's Roofs - Biomes O' Plenty 1.0 -> 1.3
> + + mcjtylib 6.0.16 -> 6.0.17
> + + MineColonies 1.0.992-ALPHA -> 1.0.1072-ALPHA
> + + Mob Grinding Utils 0.4.44 -> 0.4.46
> + + Mysterious Mountain Lib 1.0.5 -> 1.1.3
> + + MysticalAgriculture 5.1.1 -> 5.1.2
> + + Pam's HarvestCraft 2 - Food Extended 1.0.4 -> 1.0.5
> + + Placebo 6.6.4 -> 6.6.5
> + + PneumaticCraft: Repressurized 3.4.0-124 -> 3.4.3-8
> + + Polymorph 0.44 -> 0.45
> + + Potions Master 0.5.8 -> 0.5.9
> + + Productive Bees 0.9.0.11 -> 0.9.1.6
> + + RFTools Base 3.0.10 -> 3.0.11
> + + RFTools Builder 4.0.14 -> 4.0.16
> + + RFTools Control 5.0.6 -> 5.0.8
> + + RFTools Dimensions 8.0.8 -> 8.0.9
> + + RFTools Power 4.0.6 -> 4.0.7
> + + RFTools Storage 3.0.9 -> 3.0.11
> + + RFTools Utility 4.0.17 -> 4.0.19
> + + Rhino 1.14-build.225 -> 2.1-build.240
> + + Simply Light 1.4.2-build.31 -> 1.4.5-build.43
> + + Sophisticated Backpacks 3.18.21.691 -> 3.18.29.718
> + + Sophisticated Core 0.5.6.108 -> 0.5.13.132
> + + Sophisticated Storage 0.5.9.130 -> 0.5.17.152
> + + Supplementaries 1.4.12 -> 1.5.9
> + + Tetra 4.9.3 -> 4.10.0
> + + Thermal Cultivation 1.6.3.11 -> 9.0.0.13
> + + Thermal Expansion 1.6.3.13 -> 9.0.0.15
> + + Thermal Foundation 1.6.3.28 -> 9.0.0.32
> + + Thermal Innovation 1.6.3.12 -> 9.0.0.14
> + + Thermal Locomotion 1.6.3.8 -> 9.0.0.10
> + + Tinkers Construct 3.5.0.17 -> 3.5.2.40
> + + Tinkers' Integrations and Tweaks 2.0.13.0 -> 2.0.14.2
> + + Tinkers' Reforged 1.1.0 -> 1.1.3
> + + Tool's Complement 1.1.2.9 -> 2.0.0.12
> + + Xnet 4.0.6 -> 4.0.7
> + + XNet Gases 3.0.0 -> 3.0.1
> + + ZeroCore2 2.1.23 -> 2.1.26

> + Delete:
> + + Minecraft Transit Railway

> + Add:
> + + kubejs\data
> + + kubejs\server_scripts
> + + config\apotheosis
> + + config\jaopca
> + + config\titanium
> + + config\cucumber
> + + config\immersiveengineering
> + + config\quark
> + + config\tconstruct

* 2022-08-31 _ v0.1.0

> + Update:
> + + Amethyst Tools Mod 1.4.3 -> 1.6.2
> + + Blueprint 5.3.2 -> 5.4.4
> + + Gobber [FORGE] 2.6.26 -> 2.6.33
> + + Industrial Foregoing 3.3.1.3-7 -> 3.3.1.5-9
> + + KubeJS 5.4-build.492 -> 5.4-build.541
> + + MineColonies 1.0.816-ALPHA -> 1.0.992-ALPHA
> + + Placebo 6.4.0 -> 6.6.4
> + + Sophisticated Backpacks 3.17.4.596 -> 3.18.21.691
> + + Sophisticated Core 0.3.3.42 -> 0.5.6.108
> + + Sophisticated Storage 0.3.4.49 -> 0.5.9.130
> + + TerraBlender (Forge) 1.1.0.102 -> 1.2.0.126

> + Delete:
> + + Create: Steam Powered

* 2022-08-28 _ v0.0.06

> + Update:
> + + Alex's Delight 1.2.3 -> 1.3.2
> + + Alex's Mobs 1.18.5 -> 1.18.6
> + + Amethyst Tools Mod 1.4.3 -> 1.6.1
> + + Apotheosis 5.3.5 -> 5.6.1
> + + AppleSkin 2.4.0 -> 2.4.1
> + + Aquaculture 2 2.3.6 -> 2.3.8
> + + Architectury API (Fabric/Forge) 4.5.74 -> 4.9.83
> + + Balm (Forge Edition) 3.1.0+0 -> 3.2.0+0
> + + BlockUI 0.0.48-ALPHA -> 0.0.58-ALPHA
> + + Blueprint 5.3.2 -> 5.4.3
> + + Bookshelf 13.2.21 -> 13.2.49
> + + Botania 433 -> 435
> + + Cable Tiers 0.545 -> 0.547
> + + CC: Tweaked 1.100.6 -> 1.100.9
> + + Champions 2.1.5.5 -> 2.1.5.6
> + + Clumps 8.0.0+10 -> 8.0.0+15
> + + CoFH Core 1.6.3.19 -> 1.6.4.21
> + + Compact Machines 4.3.0 -> 4.5.0
> + + Compat O' Plenty 1.5.1 -> 1.5.4
> + + Configured 1.5.3 -> 1.5.4
> + + Controlling 9.0+19 -> 9.0+22
> + + Corn Delight 1.0.4 -> 1.0.5
> + + Create 0.4.1 -> 0.5.0
> + + Create Crafts & Additions 20220517a -> 20220817a
> + + Croptopia [FABRIC/FORGE] 2.0.5 -> 2.1.0
> + + CTM- ConnectedTexturesMod 1.1.4+4 -> 1.1.5+5
> + + Cucumber 5.1.1 -> 5.1.2
> + + Curios API (Forge) 5.0.7.0 -> 5.0.7.1
> + + Cyclic 1.7.1 -> 1.7.7
> + + Dark Mode Everywhere 1.0.1 -> 1.0.2
> + + Dimensional Dungeons 164 -> 166
> + + Domum Ornamentum 1.0.50-ALPHA -> 1.0.55-ALPHA
> + + Enchanting Infuser [Forge] 3.2.0 -> 3.3.2
> + + Engineer's Decor 1.1.23-b1 -> 1.1.24
> + + Explorer's Compass 1.2.1 -> 1.2.3
> + + Extra Storage 2.1.0 -> 2.2.0
> + + Extreme Reactors 2.0.45 -> 2.0.51
> + + Farmer's Respite 1.2.0 -> 1.3.0
> + + FastFurnace 6.0.3 -> 6.1.0
> + + Flywheel 0.6.2 -> 0.6.5
> + + FORGE 40.1.51 -> 40.1.74
> + + FTB Library (Forge) 3.6-build.140 -> 3.6-build.123
> + + Gobber [FORGE] 2.6.26 -> 2.6.32
> + + Gobber Delight (A Farmer's Delight Add-on) 1.0.1 -> 1.0.2
> + + Hostile Neural Networks 3.1.1 -> 3.2.1
> + + Immersive Engineering 8.0.2-149 -> 8.0.2-151
> + + Industrial Foregoing 3.3.1.3-7 -> 3.3.1.4-8
> + + IronJetpacks 5.1.2 -> 5.1.4
> + + Jade Addons 2.0.0 -> 2.4.0
> + + JAOPCA 4.1.6.6 -> 4.1.8.14
> + + JEI - Just Enough Items 10.1.0.202 -> 10.1.3.240
> + + JER - Integration 3.3.1 -> 3.3.2
> + + KubeJS 5.4-build.492 -> 5.4-build.535
> + + KubeJS Create 2.2-build.26 -> 2.2-build.28
> + + LaserIO 1.2.0 -> 1.4.3
> + + LibX 3.2.18 -> 3.2.19
> + + Little Logistics 1.2.5 -> 1.2.6
> + + Lootr 0.2.17.54 -> 0.2.19.56
> + + Macaw's Doors 1.0.6 -> 1.0.7
> + + Macaw's Fences and Walls 1.0.5 -> 1.0.6
> + + Macaw's Lights and Lamps 1.0.3 -> 1.0.4
> + + Macaw's Trapdoors 1.0.5 -> 1.0.7
> + + mcjtylib 6.0.15 -> 6.0.16
> + + Mekanism 10.2.4.464 -> 10.2.5.465
> + + Mekanism Additions 10.2.4.464 -> 10.2.5.465
> + + Mekanism Generators 10.2.4.464 -> 10.2.5.465
> + + Mekanism Tools 10.2.4.464 -> 10.2.5.465
> + + MineColonies 1.0.816-ALPHA -> 1.0.984-ALPHA
> + + Minecraft Transit Railway 3.0.0 -> 3.0.1
> + + Mob Grinding Utils 0.4.39 -> 0.4.44
> + + More Villagers 3.3.1 -> 3.3.2
> + + Multi-Piston 1.2.7-ALPHA -> 1.2.12-ALPHA
> + + Mysterious Mountain Lib 1.0.2 -> 1.0.5
> + + MysticalAgradditions 5.1.1 -> 5.1.2
> + + MysticalAgriculture 5.1.0 -> 5.1.1
> + + Nature's Compass 1.9.5 -> 1.9.7
> + + Pam's HarvestCraft 2 - Food Core 1.0.1 -> 1.0.3
> + + Pam's HarvestCraft 2 - Food Extended 1.0.0 -> 1.0.4
> + + Patchouli 70 -> 71.1
> + + Placebo 6.4.0 -> 6.6.3
> + + Prefab 1.8.2.2 -> 1.8.2.3
> + + Productive Bees 0.9.0.5 -> 0.9.0.5
> + + Quark 3.2-357 -> 3.2-358
> + + Refined Storage 1.10.2 -> 1.10.3
> + + RFTools Base 3.0.9 -> 3.0.10
> + + RFTools Builder 4.0.12 -> 4.0.14
> + + RFTools Dimensions 8.0.7 -> 8.0.8
> + + RFTools Power 4.0.5 -> 4.0.6
> + + RFTools Storage 3.0.8 -> 3.0.9
> + + RFTools Utility 4.0.16 -> 4.0.17
> + + Rhino 1.14-build.182 -> 1.14-build.225
> + + Moonlight Lib 1.17.7 -> 1.17.9
> + + Simple Magnets 1.1.7 -> 1.1.8
> + + Simple Storage Network 1.6.1 -> 1.6.2
> + + Sophisticated Backpacks 3.17.4.596 -> 3.18.17.680
> + + Sophisticated Core 0.3.3.42 -> 0.5.1.98
> + + Sophisticated Storage 0.3.4.49 -> 0.5.1.107
> + + Structurize 1.0.411-ALPHA -> 1.0.424-ALPHA
> + + SuperMartijn642's Config Lib 1.1.3 -> 1.1.6
> + + SuperMartijn642's Core Lib 1.0.18 -> 1.0.19
> + + Supplementaries 1.4.6 -> 1.4.12
> + + Tesseract 1.0.26 -> 1.0.27
> + + Tetra 4.9.2 -> 4.9.3
> + + The Veggie Way [FORGE] 2.4.11 -> 2.4.12
> + + Thermal Cultivation 1.6.1.9 -> 1.6.3.11
> + + Thermal Expansion 1.6.1.11 -> 1.6.3.13
> + + Thermal Foundation 1.6.2.27 -> 1.6.3.28
> + + Thermal Innovation 1.6.1.10 -> 1.6.3.12
> + + Thermal Locomotion 1.6.0.6 -> 1.6.3.8
> + + Tinkers' Integrations and Tweaks 2.0.0.3 -> 2.0.13.0
> + + Tinkers' Reforged 1.0.5 -> 1.1.0
> + + Tool's Complement 1.1.1.8 -> 1.1.2.9
> + + Xnet 4.0.5 -> 4.0.6
> + + XP Tome 2.1.5 -> 2.1.6
> + + YUNG's Better Mineshafts (Forge) 2.1.3 -> 2.2
> + + ZeroCore2 2.1.19 -> 2.1.23

> + Add:
> + + FluxNetworks 7.0.7.8
> + + Cosmos Library 5.1.8.10
> + + Dimensional Pockets II 6.1.2.0
> + + Pam's HarvestCraft 2 - Crops 1.0.4
> + + Pam's HarvestCraft 2 - Trees 1.0.3
> + + Prism 1.0.1
> + + Kotlin for Forge 3.7.1

* 2022-06-18 _ v0.0.05

> + Update:
> + + Alex's Mobs 1.18.3 -> 1.18.5
> + + Architectury API (Fabric/Forge) 4.4.70 -> 4.5.74
> + + Balm (Forge Edition) 3.0.3+0 -> 3.1.0+0
> + + Biomes O' Plenty 16.0.0.118 -> 16.0.0.134
> + + Bookshelf 13.2.20 -> 13.2.21
> + + CC: Tweaked 1.100.5 -> 1.100.6
> + + Champions 2.1.5.4 -> 2.1.5.5
> + + Clumps 8.0.0+8 -> 8.0.0+10
> + + CookingForBlockheads 12.0.1 -> 12.0.2
> + + DarkUtilities 10.0.4 -> 10.0.5
> + + Dimensional Dungeons 160 -> 164
> + + Farmer's Delight 1.1.1 -> 1.1.2
> + + Farming for Blockheads 10.0.1 -> 10.0.2
> + + FORGE 40.1.48 -> 40.1.51
> + + Industrial Foregoing 3.3.1.1-3 -> 3.3.1.3-7
> + + JEI - Just Enough Items 9.7.0.196 -> 10.1.0.202
> + + JER - Just Enough Resources 0.14.1.170 -> 0.14.1.171
> + + KubeJS 5.4-build.480 -> 5.4-build.492
> + + KubeJS Thermal 1.5-build.10 -> 1.5-build.14
> + + LaserIO 1.1.1 -> 1.2.0
> + + Lexicon 1.0.0 -> 1.0.1
> + + LibX 3.2.17 -> 3.2.18
> + + Mantle 1.9.20 -> 1.9.27
> + + mcjtylib 6.0.14 -> 6.0.15
> + + Mekanism 10.2.3.463 -> 10.2.4.464
> + + Mekanism Additions 10.2.3.463 -> 10.2.4.464
> + + Mekanism Generators 10.2.3.463 -> 10.2.4.464
> + + Mekanism Tools 10.2.3.463 -> 10.2.4.464
> + + MineColonies 1.0.798-ALPHA -> 1.0.816-ALPHA
> + + More Villagers 3.2.0 -> 3.3.1
> + + Patchouli 69 -> 70
> + + PneumaticCraft: Repressurized 3.2.4-93 -> 3.3.0-99
> + + RFTools Base 3.0.8 -> 3.0.9
> + + RFTools Dimensions 8.0.6 -> 8.0.7
> + + Rhino 1.13-build.179 -> 1.14-build.182
> + + Selene Lib 1.17.4 -> 1.17.7
> + + Sophisticated Backpacks 3.17.2.588 -> 3.17.4.596
> + + Sophisticated Core 0.2.8.35 -> 0.3.3.42
> + + Sophisticated Storage 0.1.1.35 -> 0.3.4.49
> + + SuperMartijn642's Config Lib 1.1.2 -> 1.1.3
> + + Supplementaries 1.4.2 -> 1.4.6
> + + Tinkers Construct 3.5.0.17 -> 3.5.0.17
> + + ZeroCore2 2.1.17 -> 2.1.19

> + Add:
> + + Toolbelt 1.18.8
> + + Shrink. 1.3.3

* 2022-06-09 _ v0.0.04

> + Update:
> + + Architectury API (Fabric/Forge) 4.4.68 -> 4.4.70
> + + Biomes O' Plenty 16.0.0.115 -> 16.0.0.118
> + + BlockUI 0.0.47-ALPHA -> 0.0.48-ALPHA
> + + Bookshelf 13.1.16 -> 13.2.20
> + + Botania 432 -> 433
> + + Croptopia [FABRIC/FORGE] 2.0.3 -> 2.0.5
> + + Cyclic 1.7.0 -> 1.7.1
> + + Enchantment Descriptions 10.0.3 -> 10.0.4
> + + Engineer's Decor 1.1.22 -> 1.1.23-b1
> + + Extreme Reactors 2.0.42 -> 2.0.45
> + + Extreme sound muffler (Forge) 3.24 -> 3.27
> + + FORGE 40.1.25 -> 40.1.48
> + + FTB Chunks (Forge) 3.6-build.145 -> 3.6-build.147
> + + FTB Library (Forge) 3.6-build.119 -> 3.6-build.123
> + + Immersive Engineering 8.0.1-147 -> 8.0.2-149
> + + Inventory Profiles Next 1.4.0 -> 1.5.0
> + + Iron Furnaces [FORGE] 3.3.0 -> 3.3.1
> + + Jade 5.2.0 -> 5.2.1
> + + JEED - Just Enough Effect Descriptions 1.10 -> 1.11
> + + JEI - Just Enough Items 9.7.0.195 -> 9.7.0.196
> + + Journeymap 5.8.4 -> 5.8.5
> + + JourneyMap Integration 0.9-49 -> 0.11-75
> + + KubeJS 5.3-build.444 -> 5.4-build.480
> + + KubeJS Create 2.2-build.23 -> 2.2-build.26
> + + LaserIO 1.0.2 -> 1.1.1
> + + Lexicon 0.5.0 -> 1.0.0
> + + Little Logistics 1.2.3 -> 1.2.5
> + + Lootr 0.2.16.53 -> 0.2.17.54
> + + Macaw's Fences and Walls 1.0.4 -> 1.0.5
> + + Materialis 2.5.3 -> 2.6.1
> + + mcjtylib 6.0.13 -> 6.0.14
> + + Mekanism 10.2.1.461 -> 10.2.3.463
> + + Mekanism Additions 10.2.1.461 -> 10.2.3.463
> + + Mekanism Generators 10.2.1.461 -> 10.2.3.463
> + + Mekanism Tools 10.2.1.461 -> 10.2.3.463
> + + MineColonies 1.0.765-ALPHA -> 1.0.798-ALPHA
> + + More Minecarts and Rails 1.4.5 -> 1.4.6
> + + Patchouli 67 -> 69
> + + Pipez 1.1.4 -> 1.1.5
> + + PneumaticCraft: Repressurized 3.2.3-84 -> 3.2.4-93
> + + Productive Bees 0.9.0.4 -> 0.9.0.5
> + + Repurposed Structures (Forge) 5.1.9 -> 5.1.14
> + + RFTools Base 3.0.7 -> 3.0.8
> + + RFTools Builder 4.0.10 -> 4.0.12
> + + RFTools Control 5.0.5 -> 5.0.6
> + + RFTools Dimensions 8.0.5 -> 8.0.6
> + + RFTools Utility 4.0.15 -> 4.0.16
> + + Rhino 1.13-build.175 -> 1.13-build.179
> + + Selene Lib 1.16.2 -> 1.17.4
> + + Sophisticated Core 0.2.4.26 -> 0.2.8.35
> + + Structurize 1.0.399-RELEASE -> 1.0.411-ALPHA
> + + SuperMartijn642's Config Lib 1.0.9 -> 1.1.2
> + + Supplementaries 1.4.0 -> 1.4.2
> + + TerraBlender (Forge) 1.1.0.101 -> 1.1.0.102
> + + Tetra 4.9.1 -> 4.9.2
> + + ZeroCore2 2.1.16 -> 2.1.17

> + Add:   
> + + Torchmaster 18.1.0  
> + + Industrial Foregoing 3.3.1.1-3  
> + + titanium 3.5.6-38  

* 2022-05-28 _ v0.0.03

> + Update:
> + + Architectury API (Fabric/Forge) 4.4.60 -> 4.4.68
> + + Bookshelf 13.0.14 -> 13.1.16
> + + Compact Machines 4.2.0 -> 4.3.0
> + + JAMD (Just another mining Dimension) 1.7.0-build.24 -> 1.7.0-build.25
> + + JER - Just Enough Resources 0.14.1.167 -> 0.14.1.170
> + + KubeJS 5.3-build.444 -> 5.3-build.454
> + + LaserIO 1.0.1 -> 1.0.3
> + + Mekanism 10.2.0.459 -> 10.2.1.461
> + + Mekanism Additions 10.2.0.459 -> 10.2.1.461
> + + Mekanism Generators 10.2.0.459 -> 10.2.1.461
> + + Mekanism Tools 10.2.0.459 -> 10.2.1.461
> + + Sophisticated Backpacks 3.16.4.584 -> 3.17.2.588
> + + Sophisticated Core 0.1.0.19 -> 0.2.4.26
> + + Sophisticated Storage 0.0.11.26 -> 0.1.1.35
> + + Tinkers' Reforged 1.0.2 -> 1.0.5

> + Delete:
> + + AE2 Things [Forge]
> + + Applied Energistics 2
> + + Applied Energistics 2 Wireless Terminals
> + + Applied Mekanistics
> + + Builder's Delight (Forge)
> + + Chunk Loaders
> + + Cloth Config API (Fabric/Forge)
> + + Easy Emerald Tools & More [FORGE]
> + + Easy Steel & More [FORGE]
> + + EnderChests
> + + EnderTanks
> + + Functional Storage
> + + Iron Chests
> + + ItemZoom
> + + Powder Power [FORGE]
> + + Reliquary Reincarnations
> + + ShetiPhianCore
> + + titanium
> + + Travel Anchors

* 2022-05-26 _ v0.0.02

> + Update:
> + + Applied Mekanistics 1.1.0 -> 1.1.1
> + + Architectury API (Fabric/Forge) 4.4.60 -> 4.4.64
> + + Biomes O' Plenty 16.0.0.111-universal -> 16.0.0.115
> + + Bookshelf 13.0.13 -> 13.0.14
> + + Catalogue 1.6.1 -> 1.6.2
> + + CoFH Core 1.6.1.15 -> 1.6.3.19
> + + Create: Steam Powered 2.0.2-alpha -> 2.0.3-alpha
> + + Croptopia [FABRIC/FORGE] 2.0.1 -> 2.0.3
> + + Cucumber 5.1.0 -> 5.1.1
> + + Dimensional Dungeons 154 -> 160
> + + Easy Emerald Tools & More [FORGE] 1.3.12 -> 1.3.13
> + + Easy Steel & More [FORGE] 1.5.17 -> 1.5.18
> + + FORGE 40.1.20 -> 40.1.25
> + + Iceberg 1.0.40 -> 1.0.44
> + + Inventory Profiles Next 1.3.8 -> 1.4.0
> + + IronJetpacks 5.1.1 -> 5.1.2
> + + Jade 5.1.0 -> 5.2.0
> + + JAOPCA 4.1.5.5 -> 4.1.6.6
> + + JER - Integration 3.3.0 -> 3.3.1
> + + KubeJS 5.2-build.428 -> 5.3-build.444
> + + KubeJS Create 2.1-build.16 -> 2.2-build.23
> + + Lexicon 0.3.0 -> 0.5.0
> + + Lootr 0.2.16.52 -> 0.2.16.53
> + + MineColonies 1.0.754-ALPHA -> 1.0.765-ALPHA
> + + Mob Grinding Utils 0.4.37 -> 0.4.39
> + + Powder Power [FORGE] 3.5.17 -> 3.5.18
> + + Puzzles Lib [Forge] v3.3.3 -> v3.3.5
> + + Quark 3.2-356 -> 3.2-357
> + + Repurposed Structures (Forge) 5.1.8 -> 5.1.9
> + + RFTools Dimensions 8.0.4 -> 8.0.5
> + + RFTools Utility 4.0.14 -> 4.0.15
> + + Rhino 1.12-build.171 -> 1.13-build.175
> + + Selene Lib 1.15.8 -> 1.16.2
> + + Sophisticated Storage 0.0.11.26 -> 0.0.16.30
> + + Supplementaries 1.3.7 -> 1.4.0
> + + TerraBlender (Forge) 1.1.0.99 -> 1.1.0.101
> + + The Veggie Way [FORGE] 2.4.10 -> 2.4.11
> + + Thermal Cultivation 1.6.0.8 -> 1.6.1.9
> + + Thermal Expansion 1.6.0.8 -> 1.6.1.11
> + + Thermal Foundation 1.6.0.20 -> 1.6.2.27
> + + Thermal Innovation 1.6.0.6 -> 1.6.1.10
> + + Tinkers' Reforged 1.0.2 -> 1.0.4
> + + titanium 3.5.0-32 -> 3.5.2-34
> + + Tool's Complement 1.1.0.7 -> 1.1.1.8
> + + Upgraded Netherite 4.2.0.4-beta -> 4.2.0.6-release

> + Add:
> + + Akashic Tome 1.5-20
> + + KubeJS Thermal 1.5-build.10
> + + XNet Gases 3.0.0
> + + Toolbelt 1.18.8
> + + Champions 2.1.5.4
> + + Time in a bottle standalone 2.1.0
> + + LaserIO 1.0.1
> + + mutil 4.5.0
> + + Tetra 4.9.1
> + + More Dragon Eggs 3.1
> + + Refined Storage: Requestify 2.2.0
> + + Cable Tiers 0.545

> + Delete:
> + + Eccentric Tome

* 2022-05-18 _ v001

> + change README.md
> + add CHANGELOG.md
> + add .gitignore