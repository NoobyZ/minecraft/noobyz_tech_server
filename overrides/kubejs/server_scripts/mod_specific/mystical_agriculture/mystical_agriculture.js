// priority: 100

// remove unused Seeds and Essence
/*
onEvent('recipes', e => {
    [
        'rubber',
        'mithril',
        'tungsten',
        'chrome',
        'iridium',
        'titanium',
        'platinum',
    ].forEach(name => {
        ['seeds', 'essence'].forEach(type => {
            e.remove({ output: `mysticalagriculture:${name}_${type}` })
            e.remove({ input: `mysticalagriculture:${name}_${type}` })
        })
    })
})
*/