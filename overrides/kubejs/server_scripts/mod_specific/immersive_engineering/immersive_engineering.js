// priority: 100

// Cloche Fertilizer
onEvent('recipes', e => {
    e.recipes.immersiveengineering.fertilizer('industrialforegoing:fertilizer').growthModifier(1.15)
    // minecraft:Bone Meal Modifier 1.25
    e.recipes.immersiveengineering.fertilizer('thermal:phytogro').growthModifier(1.45)
    e.recipes.immersiveengineering.fertilizer('mysticalagriculture:mystical_fertilizer').growthModifier(1.55)
})
